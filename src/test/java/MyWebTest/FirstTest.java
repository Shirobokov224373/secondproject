package MyWebTest;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class FirstTest {

        private static WebDriver driver;
        @BeforeClass
        public static void setup() {
            System.setProperty("webdriver.chrome.driver", "C:/Users/22437/IdeaProjects/chromedriver_win32/chromedriver.exe");
            driver = new ChromeDriver();
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.get("https://gmail.com");
        }
            @Test
            public void userLogin() {
                WebElement loginField = driver.findElement(By.id("identifierId"));
                loginField.sendKeys("testovm54@gmail.com");
                WebElement loginButton = driver.findElement(By.id("identifierNext"));
                loginButton.click();
                WebElement passwordField = driver.findElement(By.name("password"));
                passwordField.sendKeys("TestPass54!");
                WebElement passwordButton = driver.findElement(By.id("passwordNext"));
                passwordButton.click();

                WebElement writeButton = driver.findElement(By.xpath("//div[text()='Написать']"));
                writeButton.click();
                WebElement toField = driver.findElement(By.name("to"));
                toField.sendKeys("TestTo1234567890@test.ru");
                WebElement aboutField = driver.findElement(By.name("subjectbox"));
                aboutField.sendKeys("TestAbout1234567890");
                WebElement bodyField = driver.findElement(By.xpath("//div[4]/table/tbody/tr/td[2]/table/tbody/tr[1]/td/div/div/div[2]/div/div/table/tbody/tr/td/div[2]/div"));
                bodyField.sendKeys("TestBody1234567890");
//      Проверяем черновики
                driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[1]/div[1]/div[2]/div/div/div/div[2]/div/div[1]/div[1]/div/div[5]/div/div/div[2]/span/a")).click();
                driver.findElement(By.xpath("//div[2]/div/div[2]/div/div/div/div/table/tbody/tr/td[2]/img[3]")).click();
                WebElement testToField = driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[1]/div[4]/header/div[2]/div[2]/div/form/div/input"));
                testToField.sendKeys("                 TestTo1234567890@test.ru", Keys.ENTER);
//                WebElement testAboutText = driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[4]/div[4]/div[1]/div/table/tbody/tr/td[6]/div/div/div/span/span"));

                driver.findElement(By.xpath("//div/div[3]/div[1]/div/table/tbody/tr[1]/td[6]/div/div/span")).click();
                WebElement testAboutText = driver.findElement(By.name("subjectbox"));
                String testAbout = testAboutText.getText();
                Assert.assertEquals("TestBody1234567890", testAbout);

                WebElement testBodyText = driver.findElement(By.xpath("//div/div/div/div/table/tbody/tr/td[2]/div[2]/div"));
                String testBody = testBodyText.getText();
                Assert.assertEquals("TestBody1234567890", testBody);

//      Отправляем черновик
//                driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[4]/div[4]/div[1]/div/table/tbody/tr/td[5]/div[2]/span/span")).click();
                driver.findElement(By.xpath("//div[4]/div[4]/div[1]/div/table/tbody/tr/td[5]/div[2]/span/span")).click();
                driver.findElement(By.xpath("//*[@id=\":7w\"]")).click();
//      Проверяем отправленные
//                driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[1]/div[1]/div[2]/div/div/div/div[2]/div/div[1]/div[1]/div/div[4]/div/div/div[2]/span/a")).click();
                driver.findElement(By.xpath("//div[2]/div/div[1]/div[1]/div/div[4]/div/div/div[2]/span/a")).click();
                WebElement searchToField = driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[1]/div[4]/header/div[2]/div[2]/div/form/div/input"));
                searchToField.sendKeys("TestTo1234567890@test.ru");
//                WebElement sendAboutText = driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[4]/div[4]/div[1]/div/table/tbody/tr/td[6]/div/div/div/span/span"));
                WebElement sendAboutText = driver.findElement(By.xpath("/div[2]/div/div[1]/div/div[4]/div[4]/div[1]/div/table/tbody/tr/td[6]/div/div/div/span/span"));
                String sendAbout = sendAboutText.getText();
                Assert.assertEquals("TestAbout1234567890", sendAbout);
//                WebElement sendBodyText = driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div[1]/div/div[4]/div[4]/div[1]/div/table/tbody/tr/td[6]/div/div/span"));
                WebElement sendBodyText = driver.findElement(By.xpath("/div[2]/div/div[1]/div/div[4]/div[4]/div[1]/div/table/tbody/tr/td[6]/div/div/span"));
                String sendBody = sendBodyText.getText();
                Assert.assertEquals("TestBody1234567890", sendBody);


                driver.findElement(By.xpath("/html/body/div[7]/div[3]/div/div[1]/div[4]/header/div[2]/div[3]/div/div[2]/div/a/span")).click();
                driver.findElement(By.xpath("//*[@id=\"gb_71\"]")).click();

//                WebElement profileUser = driver.findElement(By.cssSelector(".login-button__user"));
//                String mailUser = profileUser.getText();
//                Assert.assertEquals("autotestorgua@ukr.net", mailUser);
        }
    @AfterClass
    public static void tearDown() {
        WebElement menuUser = driver.findElement(By.cssSelector(".login-button__menu-icon"));
        menuUser.click();
        WebElement logoutButton = driver.findElement(By.id("login__logout"));
        logoutButton.click();
        driver.quit();
    }
}
